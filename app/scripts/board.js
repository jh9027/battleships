class Board {
  constructor() {
    // Create a 10 by 10 array of cells to store game data
    this.cells = ko.observableArray(Array(10).fill().map(() => {
      return ko.observableArray(Array(10).fill().map(() => new Cell()));
    }));
  }

  addShip(col, row, ship, orientation, player) {
    var x = parseInt(col, 10),
        y = parseInt(row, 10) - 1; // Y is input 1 based

    // If the ship would overflow the board boundary return false
    console.log(orientation, x, y, ship.size)
    if ((orientation === "horizontal" && (x + ship.size > 10)) ||
        (orientation === "vertical" && (y + ship.size > 10))) {
      return false;
    }

    for (let i = 0; i < ship.size; i++) {
      let tmpX = x;
      let tmpY = y;

      if (orientation === "horizontal") {
        tmpX += i;
      } else {
        tmpY += i
      }
      if (player === PLAYER) {
        this.cells()[tmpY]()[tmpX].addPlayerShip();
      } else {
        this.cells()[tmpY]()[tmpX].addCompShip();
      }
    }
    return true;
  }

  getPlayerShipsRemaining() {
    return this.getShipsRemaining((cum, cell) => {
      return cum += (cell.isPlayerShip() && cell.isCompHit()) ? 0 : 1;
    });
  }

  getCompShipsRemaining() {
    return this.getShipsRemaining((cum, cell) => {
      return cum += (cell.isCompShip() && cell.isPlayerHit()) ? 0 : 1;
    });
  }

  getShipsRemaining(fn) {
    var rowCount = this.cells().length,
        cum = 0;
    for (let i = 0; i < rowCount; i++) {
      cum += this.cells()[i]().reduce(fn, 0);
    }
    return cum;
  }

}