const cellRefRegex = /^([A-J])([1-9]|10)$/i;
const letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"];

// the primary view model
class GameView {
  constructor(board) {
    // Array of ships each player has
    this.validShips = ko.observableArray([
      { "type": "Aircraft Carrier", "size": 5 },
      { "type": "Battleship", "size": 4 },
      { "type": "Submarine", "size": 3 },
      { "type": "Destroyer", "size": 3 },
      { "type": "Patrol Boat", "size": 2 }
    ])

    this.board = new Board();
    this.generateComputerBoard();

    this.started = ko.observable(false);

    // Variables to store the ship adding form state
    this.selectedShip = ko.observable();
    this.selectedOrientation = ko.observable();
    this.startCell = ko.observable();
  }

  // Generate a random board for the computer player
  generateComputerBoard() {
    for (let ship of this.validShips()) {
      let added = false;

      while (!added) {
        let x = Math.floor(Math.random() * 9);
        let y = Math.floor(Math.random() * 9);
        added = this.board.addShip(x, y, ship, "horizontal", COMP);
      }
    }
  }

  // Place a ship on the board
  placeShip() {
    if (!this.selectedShip() || !this.selectedOrientation() ||
        !cellRefRegex.test(this.startCell())) {
      alert("Invalid input, try again!");
      return;
    }

    var [_, col, row] = this.startCell().match(cellRefRegex),
      ship = this.selectedShip(),
      orientation = this.selectedOrientation(),
      added = false;

    // Calculate array offset for letter
    col = letters.indexOf(col.toLowerCase());

    added = this.board.addShip(col, row, ship, orientation, PLAYER);
    if (!added) {
      alert("Invalid ship position, size overflows board boundary");
      return;
    }
    this.validShips.remove(this.selectedShip());

    if (!this.validShips().length) {
      this.started(true);
    }
  }

  // Record a move by the player
  playerMove(cell) {
    if (!this.started()) {
      alert("You must place all your battleships before the game can start!");
      return;
    }

    cell.playerHit();
    if (this.board.getPlayerShipsRemaining() === 0) {
      alert("Hooray, you've won!");
      return;
    }

    this.computerMove();
    if (this.board.getCompShipsRemaining() === 0) {
      alert("Oooops, you lose.");
      return;
    }
  }

  computerMove() {
    let x = Math.floor(Math.random() * 9);
    let y = Math.floor(Math.random() * 9);
    this.board.cells()[y]()[x].compHit();
  }
}