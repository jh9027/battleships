// Constants for the player and computer offsets
const PLAYER = 0;
const COMP = 1;

// Stores data for a game cell (e.g. has ships, has hit)
class Cell {
  constructor(ships = [false, false]) {
    // Array to store which players, if any, have ships on this cell
    this.ships = ko.observableArray(ships);
    // Array to store which players, if any, have hit this cell
    this.hit = ko.observableArray([false, false]);
  }

  addPlayerShip() {
    this.ships()[PLAYER] = true;
    this.ships.valueHasMutated()
  }

  addCompShip() {
    this.ships()[COMP] = true;
    this.ships.valueHasMutated()
  }

  // Record a player hit
  playerHit() {
    this.hit()[PLAYER] = true;
    this.hit.valueHasMutated();
  }

  // Record a computer hit
  compHit() {
    this.hit()[COMP] = true;
    this.hit.valueHasMutated();
  }

  // Returns if the player has hit this cell
  isPlayerHit() {
    return this.hit()[PLAYER];
  }

  // Returns if the computer has hit this cell
  isCompHit() {
    return this.hit()[COMP];
  }

  // Returns if the player has a ship on this cell
  isPlayerShip() {
    return this.ships()[PLAYER];
  }

  // Returns if the computer has a ship on this cell
  isCompShip() {
    return this.ships()[COMP];
  }
}