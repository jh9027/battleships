(function() {

  describe('Board', function() {
    it('has 10 cells', function() {
      const board = new Board();
      assert.equal(board.cells.length, 10);
    });
  });
})();
