# Battleships Sample Task

## Prerequisites

The following is a rough list of prerequisites. You may have success with alternate versions but this is what has been tested:

- Node v4 or higher
- npm 3.10 or higher
- The following npm packages:
  - gulp-cli
  - bower

## Usage

1. Run `npm install && bower install`
2. Run `gulp serve` to run the development server